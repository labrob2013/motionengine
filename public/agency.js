// Agency Theme JavaScript

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function(){
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    $.ajax({
        url: $('#script-handle').data('api'),
        type: "GET",
        success: function(commits, code){
            commits.sort(function(a, b){
                return new Date(b.date) - new Date(a.date);
            });

            var datas = "";
            $.each(commits, function(i, item){
                datas += '<tr><td>'+item.project+'</td><td>'+item.date+'</td><td>'+item.author+'</td><td>'+item.description+'</td></tr>';
            });
            $("#commits tr").remove();
            $("#commits").append(datas);
        },
        error: function(content, code){
            console.log(content);
        }
    });

})(jQuery); // End of use strict
